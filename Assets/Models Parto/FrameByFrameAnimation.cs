﻿using UnityEngine;
using System.Collections;

public class FrameByFrameAnimation : MonoBehaviour 
{
	public bool loop = true;
	public bool play = true;
	public float FPS = 60;
	float frameTime = 0;

	int currentFrame = -1;
	Transform[] frames;

	public int Frame
	{
		get
		{
			return currentFrame;
		}
		set
		{
			if(value >= 0 && value < frames.Length)
			{
				frames[currentFrame].gameObject.SetActive(false);
				currentFrame = value;
				frames[currentFrame].gameObject.SetActive(true);
			}
		}
	}

	void Start()
	{
		frames = new Transform[this.transform.childCount];
		for(int i = 0; i < frames.Length; i++)
		{
			frames[i] = this.transform.GetChild(i);
			frames[i].gameObject.SetActive(false);
		}
	}

	void Update () 
	{
		if(play)
		{
			if(currentFrame != -1)
			{
				frames[currentFrame].gameObject.SetActive(false);
			}
            else
            {
                currentFrame = 0;
            }
			if(FPS <= 0) FPS = 0.1f;
			if(frameTime > (1/FPS))
			{
				currentFrame++;
				frameTime = 0f;
			}
			frameTime += Time.deltaTime;
	
			if(currentFrame >= frames.Length)
			{
				currentFrame = 0;
				if(!loop)
				{
					play = false;
				}
			}
			frames[currentFrame].gameObject.SetActive(true);


		}
	}
}
