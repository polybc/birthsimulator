﻿using UnityEngine;
using System.Collections;

using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;

public class ModelRotation : MonoBehaviour {

	float speed;
	public GameObject myo = null;
	private Pose _lastPose = Pose.Unknown;
	Vector3 meshesCenter;
    FrameByFrameAnimation animationScript;
	
	void Start () {
		speed = 50f;
		meshesCenter = GetCenterOfMultipleBounds();

        animationScript = gameObject.GetComponentInChildren<FrameByFrameAnimation>();
    }

	void Update () {
        // keyboard rotation
		if (Input.GetKey(KeyCode.RightArrow)){
			gameObject.transform.RotateAround(meshesCenter, Vector3.up, speed*Time.deltaTime);
		}
		else if (Input.GetKey(KeyCode.LeftArrow)){
			gameObject.transform.RotateAround(meshesCenter, Vector3.up, -speed*Time.deltaTime);
		}

        // myo rotation
		ThalmicMyo thalmicMyo = myo.GetComponent<ThalmicMyo> ();
		if (thalmicMyo.arm.ToString() == "Right"){
			if (thalmicMyo.pose == Pose.WaveIn) {
				gameObject.transform.RotateAround(meshesCenter, Vector3.up, speed*Time.deltaTime);
			}
			else if (thalmicMyo.pose == Pose.WaveOut){
				gameObject.transform.RotateAround(meshesCenter, Vector3.up, -speed*Time.deltaTime);
			}
		}
		else if (thalmicMyo.arm.ToString() == "Left"){
			if (thalmicMyo.pose == Pose.WaveIn) {
				gameObject.transform.RotateAround(meshesCenter, Vector3.up, -speed*Time.deltaTime);
			}
			else if (thalmicMyo.pose == Pose.WaveOut){
				gameObject.transform.RotateAround(meshesCenter, Vector3.up, speed*Time.deltaTime);
			}
		}

        //myo resume/pause animation
        if (thalmicMyo.pose == Pose.DoubleTap)
        {
            animationScript.play = !animationScript.play;
        }
    }

	Vector3 GetCenterOfMultipleBounds(){
		Bounds bigBound = new Bounds();
		foreach (Renderer r in GetComponentsInChildren<Renderer>()){
			bigBound.Encapsulate(r.bounds);
		}
		return bigBound.center;
	}

	//ExtendUnlockAndNotifyUserAction(thalmicMyo);
	void ExtendUnlockAndNotifyUserAction (ThalmicMyo myo)
	{
		ThalmicHub hub = ThalmicHub.instance;
		
		if (hub.lockingPolicy == LockingPolicy.Standard) {
			myo.Unlock (UnlockType.Timed);
		}
		
		myo.NotifyUserAction ();
	}
}
